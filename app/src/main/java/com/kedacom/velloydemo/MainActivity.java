package com.kedacom.velloydemo;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kedacom.velloydemo.bean.Result;
import com.kedacom.velloydemo.bean.WeatherList;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView resultView;
    ImageView mImageView;
    private NetworkImageView mNetworkImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultView = findViewById(R.id.tv_result);
        mImageView = findViewById(R.id.iv_image);
        mNetworkImageView = findViewById(R.id.id_network_image);

        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
        findViewById(R.id.button5).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                testStringRequest();
                break;
            case R.id.button2:
                testJsonRequest();
                break;
            case R.id.button3:
                testImageRequest();
                break;
            case R.id.button4:
                testImageLoader();
                break;
            case R.id.button5:
                testNetImageView();
                break;
        }
    }


    private void testStringRequest() {
        String url = "http://api.k780.com/?app=weather.history&weaid=1&date=2015-07-20&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=json";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("response:", response);
                if (TextUtils.isEmpty(response)) {
                    resultView.setText("null");
                } else {
                    resultView.setText("response:" + response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.getMessage());
                if (TextUtils.isEmpty(error.getMessage())) {
                    resultView.setText("null");
                } else {
                    resultView.setText("error:" + error.getMessage());
                }
            }
        }
        );
        queue.add(stringRequest);
    }


    private void testJsonRequest() {
        String url = "http://api.k780.com/?app=weather.history&weaid=1&date=2015-07-20&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=json";

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                WeatherList weatherList = new Gson().fromJson(response.toString(), WeatherList.class);
                if (weatherList != null) {
                    StringBuilder sb = new StringBuilder();
                    Log.d("##getSuccess##", "city:" + weatherList.getSuccess() + "\n");
                    for (Result r : weatherList.getResult()) {
                        Log.d("##result##", "city:" + r.getCitynm() + "weather:" + r.getWeather() + "\n");
                        sb.append("city:" + r.getCitynm() + "weather:" + r.getWeather() + "\n");
                    }

                    if (TextUtils.isEmpty(sb)) {
                        resultView.setText("null");
                    } else {
                        resultView.setText(sb.toString());
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (TextUtils.isEmpty(error.getMessage())) {
                    resultView.setText("null");
                } else {
                    resultView.setText("error:" + error.getMessage());
                }
            }
        });
        queue.add(jsonObjectRequest);
    }

    private void testImageRequest() {
        String url = "http://img3.imgtn.bdimg.com/it/u=2568996661,777819818&fm=27&gp=0.jpg";

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        ImageRequest jsonObjectRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {

            @Override
            public void onResponse(Bitmap response) {
                if (response != null) {
                    mImageView.setImageBitmap(response);
                    resultView.setText("");
                }
            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.RGB_565, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (TextUtils.isEmpty(error.getMessage())) {
                    resultView.setText("null");
                } else {
                    resultView.setText("error:" + error.getMessage());
                }
            }
        });
        queue.add(jsonObjectRequest);
    }

    private void testImageLoader() {
        String url = "https://c-ssl.duitang.com/uploads/item/201907/26/20190726090333_gitwl.thumb.700_0.gif";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        ImageLoader.ImageCache imageCache = new ImageLoader.ImageCache() {
            @Override
            public Bitmap getBitmap(String url) {
                return null;
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {

            }
        };
        ImageLoader.ImageListener imageListener = ImageLoader.getImageListener(mImageView, R.mipmap.ic_launcher, R.mipmap.ic_launcher);
        ImageLoader imageLoader = new ImageLoader(queue, imageCache);
        imageLoader.get(url, imageListener);
        resultView.setText("");
    }

    private void testNetImageView() {
        String url = "https://c-ssl.duitang.com/uploads/item/201907/15/20190715111054_orueo.thumb.700_0.jpg";
        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        ImageLoader.ImageCache imageCache = new ImageLoader.ImageCache() {
            @Override
            public Bitmap getBitmap(String url) {
                return null;
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {

            }
        };
        ImageLoader imageLoader = new ImageLoader(queue, imageCache);
        mNetworkImageView.setDefaultImageResId(R.mipmap.ic_launcher);
        mNetworkImageView.setErrorImageResId(R.mipmap.ic_launcher);
        mNetworkImageView.setImageUrl(url, imageLoader);
        resultView.setText("");
    }
}

